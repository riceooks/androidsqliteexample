package tw.edu.hust.yn97024.sqlite;

import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	private Button btn_write, btn_reader, btn_clear;
	private SQLiteDatabase db;
	private Cursor cursor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
		// 產生(讀取) SQLite
		db = openOrCreateDatabase("AndroidSQLiteExample.sqlite", MODE_PRIVATE, null);
		
		createTable("Name");
        
        // 宣告元件
        btn_write = (Button) findViewById(R.id.btn_write);
        btn_reader = (Button) findViewById(R.id.btn_reader);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        
        // 點擊事件
        btn_write.setOnClickListener(this);
        btn_reader.setOnClickListener(this);
        btn_clear.setOnClickListener(this);
    }

    private void createTable(String table) {
    	// 建立資料表
		db.execSQL("CREATE TABLE IF NOT EXISTS "+ table +" (`_id` INTEGER PRIMARY KEY AUTOINCREMENT,"
														+ "`name` VARCHAR(100) NOT NULL,"
														+ "`value` INTEEGER NOT NULL DEFAULT 0)");
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	@Override
	public void onClick(View v) {
		switch( v.getId() ){
		case R.id.btn_write:
			// 寫入資料
			db.execSQL("INSERT INTO Name(`name`) VALUES('Apple')");
			db.execSQL("INSERT INTO Name(`name`, `value`) VALUES('Barry', 24)");
			
			break;
		case R.id.btn_reader:
			// 宣告游標並讀取資料
			cursor = db.rawQuery("SELECT * FROM Name ORDER BY _id", null);
			
			// 使用迴圈讀取游標資料
			while( cursor.moveToNext() ){
				String name = cursor.getString( cursor.getColumnIndex("name") );
				int value = cursor.getInt( cursor.getColumnIndex("value") );
				
				Toast.makeText(this, String.format("name => %s, value => %d", name, value), Toast.LENGTH_SHORT).show();
			}
			
			break;
		case R.id.btn_clear:
			// 刪除資料表
			db.execSQL("DELETE FROM Name");
			
			// 建立資料表
			createTable("Name");
			
			break;
		}
	}

    
}
